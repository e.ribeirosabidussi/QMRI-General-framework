{ "Configuration-file": {
	"network-parameters": {
		"layer_channel_list": [40, 60, 80, 320, 80, 40, 6],
		"basic_block_layers": [4,4,4,4,4,4],
		"number_input_channels": 6,
		"number_output_channels": 2,
		"use_mean_batch_norm": false
	}
   }
}