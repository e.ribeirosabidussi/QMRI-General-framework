from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from abc import ABCMeta, abstractmethod


class MyMeta(ABCMeta):
    required_attributes = []
    def __call__(self, *args, **kwargs):
        obj = super(MyMeta, self).__call__(*args, **kwargs)
        for attr_name in obj.required_attributes:
            if not getattr(obj, attr_name):
                raise ValueError('required attribute (%s) not set' % attr_name)
        return obj


class BaseInferenceMethod(object, metaclass=MyMeta):
    """
        Abstract class for implementation of mapping methods.
        A derived class should implement the following methods and attributes:
            - __parseConfigFile__: Method to parse configuration settings
            - forward: Method with forward pass of the method

            - Required attributes: 'configurationFile' : Path to configuration file
    """

    required_attributes = ['configurationFile']
    def __init__(self):
        super().__init__()

    @abstractmethod
    def __parseConfigFile__(self):
        raise NotImplementedError("Configuration file parser not implemented")

    @abstractmethod
    def forward(self):
        raise NotImplementedError("Forward method not implemented")

    



    


