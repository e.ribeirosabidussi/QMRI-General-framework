from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

import argparse
import logging
from .utilities import str2bool as custom_bool


class GlobalConfig(object):
    def __init__(self, mode='training'):
        self.mode = mode.upper()
        self.parser = argparse.ArgumentParser(description='Global configuration for inference models')
        self.set_global_config()

        sp = self.parser.add_subparsers(dest='subargs')
        self.mode_parser = sp.add_parser(self.mode.upper())

        self.set_mode_config()

        self.config_args = self.parser.parse_known_args()
        self.task_parser = sp.add_parser(self.config_args[0].task.upper())

        ################################################################
        ################################################################
        ## TASK CONFIG
        if self.config_args[0].task.upper() == 'RELAXOMETRY':
            from .relaxometry.relaxometry_config import RelaxometryConfig
            RelaxometryConfig(self.task_parser)
        elif self.config_args[0].task.upper() == 'EXAMPLE':
            pass
        else:
            logging.warning('Task not specified in arguments')
        ################################################################
        ################################################################

        self.config_args = self.parser.parse_known_args()
        self.args = self.parser.parse_args(self.config_args[1], self.config_args[0])
        self.args.mode = self.mode

    def set_global_config(self):
        self.parser.add_argument('--task', type=str, help='Path to configuration file', default='relaxometry')
        self.parser.add_argument('--inferenceModel', type=str, help='DL Model (RIM, ResNet, MLE)', default='RIM')
        self.parser.add_argument('--configuration', type=str, help='Path to configuration file')
        self.parser.add_argument('--loadCheckpoint', type=custom_bool, help='If True, -loadCheckpointPath option must be set', default=False)
        self.parser.add_argument('-loadCheckpointPath', type=str, help='Path to existing model checkpoint')
        self.parser.add_argument('-usePatches', type=custom_bool, help='If True, numberOfPatches and patchSize must be specified', default=True)
        self.parser.add_argument('-patchSize', type=int, help='Training patch size', default=80)
        self.parser.add_argument('-numberOfPatches', type=int, help='Number of training patches per iteration', default=11)
        self.parser.add_argument('-batchSize', type=int, help='Size of training batch', default=3)
        self.parser.add_argument('-useCUDA', type=custom_bool, help='If True, use GPU. If False or not specified, use CPU', default=False)
        self.parser.add_argument('-runBenchmark', type=bool, help='if True (default), run GPU optimization benchmark', default=True)

    def set_mode_config(self):
        if self.mode == 'TRAINING':
            self.mode_parser.add_argument('--trainingData', type=str, help='Path to training data')
            self.mode_parser.add_argument('--runValidation', type=custom_bool, help='If True, runs validation step during training. --validationData must be specified', default=False)
            self.mode_parser.add_argument('-validationData', type=str, help='Path to validation data')
            self.mode_parser.add_argument('-preLoadData', type=custom_bool, help='If true, it will pre load all data (within data path) in memory', default=True)
            self.mode_parser.add_argument('-saveCheckpoint', type=custom_bool, help='If True, it will save a model checkpoint to the path specified in -saveCheckpointPath', default=False)
            self.mode_parser.add_argument('-saveCheckpointPath', type=str, help='Path to existing model checkpoint')
            self.mode_parser.add_argument('-loss', type=str, help='Cost function for network training (MSE)', default='MSE')
            self.mode_parser.add_argument('-epochs', type=int, help='Number of training epochs', default=10)
            self.mode_parser.add_argument('-LR', type=float, help='Learning rate for network training (default:0.0001)', default=0.0001)
            self.mode_parser.add_argument('-saveInterResults', type=custom_bool, help='If True, it will save estimates at each epoch at the specified resultsPath', default=False)
            self.mode_parser.add_argument('-resultsPath', type=str, help='Required if -saveInterResults is True')

        elif self.mode == 'TESTING':
            self.mode_parser.add_argument('--testingData', type=str, help='Specified the folder containing test data')
            self.mode_parser.add_argument('--resultsPath', type=str, help='Required if -saveInterResults is True')
            self.mode_parser.add_argument('-preLoadData', type=custom_bool, help='If true, it will pre load all data within data path', default=True)
            self.mode_parser.add_argument('-loss', type=str, help='Cost function for network training (MSE)', default='MSE')
            self.mode_parser.add_argument('-epochs', type=int, help='Number of training epochs', default=10)
            self.mode_parser.add_argument('-LR', type=float, help='Learning rate for network training (default:0.0001)', default=0.0001)
