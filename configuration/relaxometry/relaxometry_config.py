from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from ..utilities import str2bool as custom_bool
from ..global_config import GlobalConfig
import logging


class RelaxometryConfig(GlobalConfig, object):
    def __init__(self, parser):
        self.subparser = parser
        self.config()

    def config(self):
        self.subparser.add_argument('-signalModel', type=str, help='Forward model (T1, T2)', default='T1')
        self.subparser.add_argument('-useSimulatedData', type=custom_bool, help='Use synthetic data', default=True)
        self.subparser.add_argument('-likelihood', type=str, help='Likelihood function (Gaussian, Rician)', default='Gaussian')
        self.subparser.add_argument('-sigmaNoise', type=float, help='Standard deviation of simulated acquisition noise. If any value is given, all training samples will be produced with this SD. If no value is given, random SDs will be used', default=0.001)
        self.subparser.add_argument('-simulateArtefacts', type=custom_bool, help='If True, signal artefacts will be added to ground-truth maps', default=False)
        self.subparser.add_argument('-useRandomSeed', type=custom_bool, help='If True, all values generated via random sampling will be generated from the same seed', default=False)
        self.subparser.add_argument('-tau', type=float, help='Values in miliseconds. If not specified, default values are used', default=[139.864, 166.44, 193.016, 219.592, 246.168, 272.744, 299.32, 325.896, 352.472, 
                    379.048, 405.624, 432.2, 458.776, 485.352, 511.928, 538.504, 565.08, 591.656,
                    618.232, 644.808, 671.384, 697.96, 724.536, 751.112, 777.688, 804.264, 838.4, 
                    857.416, 883.992, 915.68, 937.144])

        # self.subparser.add_argument('-tau', type=float, help='Values in miliseconds. If not specified, default values are used', default=[10,20,40,80,160,320])
        