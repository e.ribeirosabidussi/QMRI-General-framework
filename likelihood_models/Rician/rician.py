from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")

from ..baseLL import BaseLikelihood
import math
import torch

class Rician(BaseLikelihood):
    """
        Class for the Rician PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise 
                inputs: a signal and sigma (SD of the noise)
                outputs: Noisy signal
    """

    def __init__(self):
        super(Rician, self).__init__()

    def logLikelihood(self, signal, sigma, mu):
        pass

    def applyNoise(self, signal, sigma):
        pass