from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

import build_model as build_model
import checkpoint
import configuration.global_config as config
import data_loader as data_loader
import logging
import numpy as np
import torch
import utils
import utility_tools.image_utilities as image_utilities


def get_batch(data, args, batch_generator):
    data_ = []
    data_.append(data[0][0])
    args.filename = data[1][0]
    if len(data)>2:
        data_.append(data[2][0])
    if len(data)>3:
        data_.append(data[3][0])

    batched_data, indexes = batch_generator.send(data_)
    args.batchSizeIter = indexes[0] - indexes[1]

    batched_data_ = []
    batched_data_.append(batched_data[0])
    if len(batched_data)>1:
        batched_data_.append(batched_data[1])
        batched_data_.append(batched_data[2].unsqueeze(1))

    return batched_data_, indexes


def train(args, inferenceModel, costFunction):
    """
        Train neural network models.
        Inputs: args (input arguments), inferenceModel (neural network model),
                costFunction (objective function for training)
        Outputs: -

        This function saves a checkpoint of the model at the end of every training epoch.
    """
    epoch = 0
    for _ in range(args.epochs):
        state = next(args.state)
        dataloader = list(state.values())[0]
        args.stateName = list(state.keys())[0]
        inferenceModel.train() if args.stateName == 'training' else inferenceModel.eval()

        if args.stateName == 'training': 
            epoch += 1

        print('')
        logging.info('Running {}'.format(args.stateName))
        logging.info('*'*50)
        
        for i, data in enumerate(dataloader):
            #TODO: if args.usePatches: #Because if we use patches, we can divide them into batches. Otherwise we the dataloader deals with batch size.
            batch_generator = utils.batch_iterator(args, state['batchSize'])
            patch_iterations = int(np.ceil(args.numberOfPatches/state['batchSize']))
            for _ in range(patch_iterations):
                next(batch_generator)
                batched_data, indexes = get_batch(data, args, batch_generator)
                signal = batched_data[0]
                if len(batched_data)>1:
                    label = batched_data[1]
                    mask = batched_data[2]

                estimate = inferenceModel.forward(signal, args)

                if isinstance(estimate, list):
                    loss = [costFunction(e*mask, label*mask) for e in estimate]
                    loss = sum(loss) / len(loss)
                    last_estimate = estimate[-1].detach().numpy()
                else:
                    loss = costFunction(estimate*mask, label*mask)
                    last_estimate = estimate.detach().numpy()
                    
                if args.stateName == 'training':
                    loss.backward()
                    args.optimiser.step()
                    args.optimiser.zero_grad()

                logging.info("Epoch: {}, State: {}, Subject: {}/{}, Patches {}/{}, Loss: {} ".format(epoch, args.stateName, i, dataloader.__len__(), indexes[0], args.numberOfPatches, loss))
                
        if args.stateName == 'training' and args.saveCheckpoint:
            checkpoint.save(args, epoch, inferenceModel)

        if args.saveInterResults:
            if not args.resultsPath:
                logging.warning('Please specify path to save intermediary results - resultsPath')
                break
            else:
                data = {}
                data['estimated'] = last_estimate[0]
                data['labels'] = label[0].detach().numpy()
                data['mask'] = mask[0].detach().numpy()
                image_utilities.saveItermediateResults(data, args)


def kill_hanging_processes():
    osPID = os.getpid()
    command = 'taskkill /F /PID ' + str(osPID)
    os.system(command)


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)
    logging.info('Setting up environment...')
    configuration = config.GlobalConfig('training')

    inferenceModel, config = build_model.make(configuration.args)
    logging.info('{} model succesfully built.'.format(config.inferenceModel))

    dataLoaders = data_loader.create_dataloader(config)
    config.state = utils.alternateTrainingState(config, dataLoaders)
    logging.info('Starting training...')
    train(config, inferenceModel, list(config.loss.values())[0])

    if not sys.platform == 'darwin':
        kill_hanging_processes()



