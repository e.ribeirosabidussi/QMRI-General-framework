from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import checkpoint
import logging
import torch


def make(args):
    """
        Build DL model based on arguments provided via console.
        Inputs: args - input arguments
        outputs: DL model and updated args
    """
    if args.useCUDA:
        if torch.cuda.is_available():
            args.device = torch.device("cuda:0")
            logging.info("Using CUDA")
        else:
            logging.warn("User selected runtime with CUDA, but CUDA is not available. Running on CPU instead.")
    else:
        args.device = torch.device("cpu")
        logging.info("Using CPU")
    torch.backends.cudnn.benchmark = args.runBenchmark

    ################################################################
    ################################################################
    ## OPTIMIZER COST FUNCTION
    if args.loss=='MSE':
        loss = torch.nn.MSELoss(reduction='mean')
    ## INSERT NEW MODEL HERE (LOOK AT THE EXAMPLES ABOVE)
    else:
        logging.warn("Only MSE loss supported. Automatically selecting to MSE loss")
    ################################################################
    ################################################################


    ################################################################
    ################################################################
    ## LIKELIHOOD MODEL
    if args.likelihood=='Gaussian':
        import likelihood_models.Gaussian.gaussian as gaussian_likelihood
        ll_model = gaussian_likelihood.Gaussian()
        args.likelihood = {args.likelihood: ll_model}
    ## INSERT NEW MODEL HERE (LOOK AT THE EXAMPLES ABOVE)
    else:
        logging.warn("Unknwon likelihood model - Gaussian or Rician are supported")
        logging.warn("Refer to docs to find how to create your own Likelihood Model")
    ################################################################
    ################################################################


    ################################################################
    ################################################################
    ## SIGNAL MODEL
    if args.signalModel=='T1': 
        import signal_models.LookLocker.looklocker as looklocker_model
        sig_model = looklocker_model.LookLocker_T1(args)
    elif args.signalModel=='T2': 
        import signal_models.FSE.fse as fse_model
        sig_model = fse_model.FSE_T2(args)
    ## INSERT NEW MODEL HERE (LOOK AT THE EXAMPLES ABOVE)
    else:
        logging.warn("Unknown signal model - LookLocker (T1) or Fast Spin Echo (T2) are supported")
        logging.warn("Refer to docs to find how to create your own signal model")
    args.signalModel = {args.signalModel: sig_model}
    ################################################################
    ################################################################


    ################################################################
    ################################################################
    ## TASK
    if args.task == 'relaxometry':
        import datasets.relaxometry.relaxometry_dataset as relaxometry_dataset
        args.dataset = relaxometry_dataset.RelaxometryDataset(args)
    elif args.task == 'EXAMPLE':
        pass
    ## INSERT NEW DATASET SPECIFIC TASK
    else:
        logging.warn("Unknown task - 'relaxometry' is supported")
        logging.warn("Refer to docs to find how to create your own task")
    ################################################################
    ################################################################


    ################################################################
    ################################################################
    ## INFERENCE MODEL
    if args.inferenceModel=='RIM':
        import mapping_methods.RIM.rim as rim_model
        nn_model = rim_model.RIM(args.configuration, sig_model, ll_model).type(torch.FloatTensor).to(args.device)
    elif args.inferenceModel=='ResNet':
        import mapping_methods.ResNet.resnet as resnet_model
        nn_model = resnet_model.RESNET(args.configuration).type(torch.FloatTensor).to(args.device)
    ## INSERT NEW MODEL HERE (LOOK AT THE EXAMPLES ABOVE)
    else:
        logging.warn("Unknown inference model - RIM and ResNet are supported")
        logging.warn("Refer to docs to find how to create your own inferenceModel")
    ################################################################
    ################################################################


    args.optimiser = torch.optim.Adam(nn_model.parameters(), lr=args.LR)
    args.loss = {args.loss: loss}

    if args.loadCheckpoint:
        logging.info("Loading model checkpoint from {}.".format(args.loadCheckpointPath))
        checkpoint.load(args.loadCheckpointPath, nn_model, args.optimiser)

    return nn_model, args